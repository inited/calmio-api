FROM abhin4v/hastatic:latest

COPY . /opt/mywebsite
WORKDIR /opt/mywebsite
EXPOSE 3000/tcp
CMD ["/usr/bin/hastatic"]
